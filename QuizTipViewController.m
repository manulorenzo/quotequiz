//
//  QuizTipViewController.m
//  QuoteQuiz
//
//  Created by manu on 14/05/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import "QuizTipViewController.h"

@interface QuizTipViewController ()

@end

@implementation QuizTipViewController

-(UILabel *)numberTipsText {
    if (!_numberTipsText) {
        _numberTipsText = [[UILabel alloc] init];
    }
    return _numberTipsText;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.tipView.text = self.tipText;
    self.numberTipsText.text = self.labelText;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)doneAction:(id)sender {
    [self.delegate quizTipDidFinish:self];
}
@end
