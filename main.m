//
//  main.m
//  QuoteQuiz
//
//  Created by manu on 11/05/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "QuoteQuizAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([QuoteQuizAppDelegate class]));
    }
}
