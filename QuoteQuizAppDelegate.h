//
//  QuoteQuizAppDelegate.h
//  QuoteQuiz
//
//  Created by manu on 11/05/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuoteQuizAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
