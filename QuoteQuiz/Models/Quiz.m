//
//  Quiz.m
//  QuoteQuiz
//
//  Created by manu on 13/05/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import "Quiz.h"

@interface Quiz()

@property (nonatomic, strong) NSString *quote;
@property (nonatomic, strong) NSString *ans1;
@property (nonatomic, strong) NSString *ans2;
@property (nonatomic, strong) NSString *ans3;

@end

@implementation Quiz

-(BOOL)checkCorrectQuestion:(NSUInteger)question forAnswer:(NSUInteger)chosenAnswer {
    NSString *ans = self.movieArray[question][@"answer"];
    if ([ans intValue] == chosenAnswer) {
        self.correctCount++;
        return YES;
    } else {
        self.incorrectCount++;
        return NO;
    }
}

-(void)nextQuestion:(NSUInteger)idx {
    self.quote = [NSString stringWithFormat:@"'%@'", self.movieArray[idx][@"quote"]];
    
    self.ans1 = self.movieArray[idx][@"ans1"];
    self.ans2 = self.movieArray[idx][@"ans2"];
    self.ans3 = self.movieArray[idx][@"ans3"];
    
    self.currentTip = self.movieArray[idx][@"tip"];
    
    if (idx == 0) {
        self.correctCount = 0;
        self.incorrectCount = 0;
        self.tipCount = 0;
    }
}

-(id)initWithQuiz:(NSString *)pListName {
    
    if (self = [super init]) {
        NSString *pListCatPath = [[NSBundle mainBundle] pathForResource:pListName ofType:@"plist"];
        self.movieArray = [NSMutableArray arrayWithContentsOfFile:pListCatPath];
        self.quizCount = [self.movieArray count];
    }
    self.tipCount = 0;
    return self;
}

@end
