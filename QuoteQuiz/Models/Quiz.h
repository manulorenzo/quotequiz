//
//  Quiz.h
//  QuoteQuiz
//
//  Created by manu on 13/05/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Quiz : NSObject

@property (nonatomic, strong) NSMutableArray *movieArray;
@property (nonatomic, assign) NSInteger correctCount;
@property (nonatomic, assign) NSInteger incorrectCount;
@property (nonatomic, assign) NSInteger quizCount;

@property (nonatomic, assign) NSInteger tipCount;
@property (nonatomic, strong) NSString *currentTip;

@property (nonatomic, readonly, strong) NSString *quote;
@property (nonatomic, readonly, strong) NSString *ans1;
@property (nonatomic, readonly, strong) NSString *ans2;
@property (nonatomic, readonly, strong) NSString *ans3;

-(id)initWithQuiz:(NSString *)pListName;
-(void)nextQuestion:(NSUInteger)idx;
-(BOOL)checkCorrectQuestion:(NSUInteger)question forAnswer:(NSUInteger)answer;

@end
