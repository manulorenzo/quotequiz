//
//  QuizTipViewController.h
//  QuoteQuiz
//
//  Created by manu on 14/05/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QuizTipViewControllerDelegate;
@interface QuizTipViewController : UIViewController

@property (nonatomic, assign) id <QuizTipViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextView* tipView;
@property (nonatomic, copy) NSString * tipText;
@property (strong, nonatomic) IBOutlet UILabel* numberTipsText;
@property (nonatomic, copy) NSString *labelText;

- (IBAction)doneAction:(id)sender;

@end

@protocol QuizTipViewControllerDelegate
- (void)quizTipDidFinish:(QuizTipViewController *)controller;
@end